package com.zerosdev.mercato.bg.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.zerosdev.mercato.bg.datamodel.User
import com.zerosdev.mercato.bg.repository.ItemDetailUserRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class ItemDetailUserViewModel(private val idUser: Int) : BaseViewModel() {

    @Inject
    lateinit var itemDetailUserRepository: ItemDetailUserRepository

    private val userLiveData: MutableLiveData<User> by lazy {
        MutableLiveData<User>().also {
            getUser()
        }
    }

    fun getUserLiveData(): LiveData<User> = userLiveData

    fun getUser() {
        scope.launch {
            userLiveData.postValue(itemDetailUserRepository.getUser(idUser))
        }
    }

}
