package com.zerosdev.mercato.bg.datamodel

import com.google.gson.annotations.SerializedName
import com.zerosdev.mercato.bg.projectmodel.Location

data class User(
    @SerializedName("id")
    val id: Int,
    val image: Int,
    val name: String,
    val lastName: String,
    val location: Location
)