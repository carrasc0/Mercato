package com.zerosdev.mercato.bg.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.zerosdev.mercato.bg.viewmodel.RelatedItemsViewModel

class RelatedItemsViewModelFactory(private val cat: Int) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RelatedItemsViewModel(cat) as T
    }

}