package com.zerosdev.mercato.bg.datamodel.response

import com.zerosdev.mercato.bg.datamodel.User

data class GetItemUserResponse(val user: User): BaseResponse()