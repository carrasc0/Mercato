package com.zerosdev.mercato.bg.datamodel

data class ChatListItem(
    val id: Int,
    val image: Int,
    val name: String,
    val itemTitle: String,
    val preview: String
)