package com.zerosdev.mercato.bg.datamodel

data class Message (val sender: Int, val nickname: Int, val body: String)