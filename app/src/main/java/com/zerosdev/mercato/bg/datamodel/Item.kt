package com.zerosdev.mercato.bg.datamodel

data class Item(
    val id: Int,
    val image: Int,
    val images: Array<String>?,
    val price: Int,
    val title: String,
    val body: String,
    val views: Int,
    val fav: Int
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Item

        if (id != other.id) return false
        if (image != other.image) return false
        if (images != null) {
            if (other.images == null) return false
            if (!images.contentEquals(other.images)) return false
        } else if (other.images != null) return false
        if (price != other.price) return false
        if (title != other.title) return false
        if (body != other.body) return false
        if (views != other.views) return false
        if (fav != other.fav) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + image
        result = 31 * result + (images?.contentHashCode() ?: 0)
        result = 31 * result + price
        result = 31 * result + title.hashCode()
        result = 31 * result + body.hashCode()
        result = 31 * result + views
        result = 31 * result + fav
        return result
    }
}