package com.zerosdev.mercato.bg.datamodel.response

import com.zerosdev.mercato.bg.datamodel.Comment
import com.zerosdev.mercato.bg.datamodel.Item

data class GetComentsResponse(val comments: MutableList<Comment>) : BaseResponse()
