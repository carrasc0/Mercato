package com.zerosdev.mercato.bg.datasource

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.zerosdev.mercato.bg.base.AppController
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.repository.UserItemsRepository
import com.zerosdev.mercato.bg.util.TAG
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

class UserItemDataSource : PageKeyedDataSource<Int, Item>() {

    @Inject
    lateinit var userItemsRepository: UserItemsRepository

    init {
        AppController.component.inject(this)
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Item>
    ) {
        GlobalScope.launch {
            try {
                val items = userItemsRepository.getUserItems(page = 1, idUser = 1 /*todo*/)
                callback.onResult(items!!, null, 2)
            } catch (exception: IOException) {
                println(exception)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        GlobalScope.launch {
            try {
                val items = userItemsRepository.getUserItems(page = params.key, idUser = 1)
                callback.onResult(items!!, params.key + 1)
            } catch (exception: IOException) {
                println(exception)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        GlobalScope.launch {
            try {
                userItemsRepository.getUserItems(page = params.key, idUser = 1)?.let {
                    callback.onResult(it, params.key - 1).also {
                        Log.d(
                            TAG(),
                            "load before $it ${params.requestedLoadSize} ${params.key}"
                        )
                    }
                }
            } catch (exception: IOException) {
                println(exception)
            }
        }
    }


}