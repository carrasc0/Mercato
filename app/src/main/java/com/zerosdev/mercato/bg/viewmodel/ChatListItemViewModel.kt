package com.zerosdev.mercato.bg.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.zerosdev.mercato.bg.datamodel.ChatListItem
import com.zerosdev.mercato.bg.datasource.ChatListItemDataSource
import com.zerosdev.mercato.bg.util.TAG

class ChatListItemViewModel : BaseViewModel() {

    private var chatItemsLiveData: LiveData<PagedList<ChatListItem>>
    private val config = PagedList.Config.Builder()
        .setInitialLoadSizeHint(25)
        .setPageSize(15)
        .setEnablePlaceholders(false)
        .build()

    init {
        chatItemsLiveData = initializedPageListBuilder(config).build()
    }

    fun getChatItems(): LiveData<PagedList<ChatListItem>> = chatItemsLiveData


    private fun initializedPageListBuilder(config: PagedList.Config): LivePagedListBuilder<Int, ChatListItem> {
        Log.d(TAG(), "initializedPageListBuilder in ChatListItemViewModel")
        val dataSourceFactory = object : DataSource.Factory<Int, ChatListItem>() {
            override fun create(): DataSource<Int, ChatListItem> {
                return ChatListItemDataSource()
            }
        }
        return LivePagedListBuilder<Int, ChatListItem>(dataSourceFactory, config)
    }

}