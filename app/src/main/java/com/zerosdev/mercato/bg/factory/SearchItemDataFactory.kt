package com.zerosdev.mercato.bg.factory

import androidx.arch.core.util.Function
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.zerosdev.mercato.bg.datasource.SearchItemDataSource
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.projectmodel.Order
import com.zerosdev.mercato.bg.projectmodel.Price

class SearchItemDataFactory(private val dep: Int, private val price: Price, val order: Order) :
    DataSource.Factory<Int, Item>() {

    private lateinit var searchItemDataSource: SearchItemDataSource
    private val mutableLiveData: MutableLiveData<SearchItemDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, Item> {
        searchItemDataSource = SearchItemDataSource(dep, price, order)
        mutableLiveData.postValue(searchItemDataSource)
        return searchItemDataSource
    }

    override fun <ToValue : Any?> map(function: Function<Item, ToValue>): DataSource.Factory<Int, ToValue> {
        return super.map(function)
    }

    override fun <ToValue : Any?> mapByPage(function: Function<MutableList<Item>, MutableList<ToValue>>): DataSource.Factory<Int, ToValue> {
        return super.mapByPage(function)
    }

    fun getMutableLiveData(): MutableLiveData<SearchItemDataSource> {
        return mutableLiveData
    }


}