package com.zerosdev.mercato.bg.api

import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.datamodel.User
import com.zerosdev.mercato.bg.datamodel.response.*
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("/getMainItems")
    fun getMainItems(
        @Query("page") page: Int
    ): Deferred<Response<GetItemsResponse>>

    @GET("/getSearchItems")
    fun getSearchItems(
        @Query("page") page: Int,
        @Query("dep") dep: Int,
        @Query("min") min: Int,
        @Query("max") max: Int,
        @Query("order") order: Int
    ): Deferred<Response<GetItemsResponse>>

    @GET("/getChatList")
    fun getChatListItems(
        @Query("page") page: Int,
        @Query("id_user") idUser: Int
    ): Deferred<Response<GetChatListItemResponse>>

    @GET("/getUserItems")
    fun getUserItems(
        @Query("page") page: Int,
        @Query("id_user") idUser: Int
    ): Deferred<Response<GetUserItemsResponse>>

    @GET("/getUserProfile")
    fun getUserProfile(
        @Query("id_user") idUser: Int
    ): Deferred<Response<User>>

    @GET("/getItem")
    fun getItem(
        @Query("id_item") idItem: Int
    ): Deferred<Response<Item>>

    @GET("/getComments")
    fun getComments(
        @Query("id_item") idItem: Int
    ): Deferred<Response<GetComentsResponse>>

    @GET("/getRelatedItems")
    fun getRelatedItems(
        @Query("cat") cat: Int
    ): Deferred<Response<GetItemsResponse>>

    @GET("/getItemViewUser")
    fun getItemViewUser(
        @Query("id_user") idUser: Int
    ): Deferred<Response<GetItemUserResponse>>

}