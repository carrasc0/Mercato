package com.zerosdev.mercato.bg.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.zerosdev.mercato.bg.R
import kotlinx.android.synthetic.main.search_tag_design.view.*

class SearchTagView(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    init {
        inflate(context, R.layout.search_tag_design, this)
        context.theme.obtainStyledAttributes(attrs, R.styleable.SearchTagView,0,0).apply {
            try {
                searchTagTextView.text = getString(R.styleable.SearchTagView_text)
                searchTagTextView.setCompoundDrawables(
                    getDrawable(R.styleable.SearchTagView_icon), null,
                    getDrawable(R.styleable.SearchTagView_action_icon), null
                )
            }finally {
                recycle()
            }
        }
    }


}