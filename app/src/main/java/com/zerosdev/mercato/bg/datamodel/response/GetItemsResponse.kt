package com.zerosdev.mercato.bg.datamodel.response

import com.zerosdev.mercato.bg.datamodel.Item

data class GetItemsResponse(val items: MutableList<Item>) : BaseResponse()
