package com.zerosdev.mercato.bg.base

import android.app.Application
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.google.firebase.FirebaseApp
import com.zerosdev.mercato.bg.dagger.component.AppComponent
import com.zerosdev.mercato.bg.dagger.component.DaggerAppComponent
import com.zerosdev.mercato.bg.dagger.modules.AppModule
import com.zerosdev.mercato.bg.dagger.modules.NetworkModule

class AppController : Application() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        FacebookSdk.sdkInitialize(this)
        AppEventsLogger.activateApp(this)
        FirebaseApp.initializeApp(this)

        component = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule("http://192.168.1.8:8000/chat", "http://192.168.1.8:3000"))
            .build()

    }

}