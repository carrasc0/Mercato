package com.zerosdev.mercato.bg.repository

import com.zerosdev.mercato.bg.api.ApiInterface
import com.zerosdev.mercato.bg.base.BaseRepository
import com.zerosdev.mercato.bg.datamodel.Item

class MainItemsRepository(private val apiInterface: ApiInterface) : BaseRepository() {

    suspend fun getMainItems(page: Int): MutableList<Item>? {
        return safeApiCall(
            call = {apiInterface.getMainItems(page).await()},
            error = "Error fetching main messages"
        )?.items?.toMutableList()
    }

}