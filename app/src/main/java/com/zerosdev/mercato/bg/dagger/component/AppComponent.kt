package com.zerosdev.mercato.bg.dagger.component

import com.zerosdev.mercato.bg.dagger.modules.AppModule
import com.zerosdev.mercato.bg.dagger.modules.NetworkModule
import com.zerosdev.mercato.bg.datasource.ChatListItemDataSource
import com.zerosdev.mercato.bg.datasource.MainItemDataSource
import com.zerosdev.mercato.bg.datasource.SearchItemDataSource
import com.zerosdev.mercato.bg.datasource.UserItemDataSource
import com.zerosdev.mercato.bg.repository.ChatRepository
import com.zerosdev.mercato.bg.repository.ItemDetailRepository
import com.zerosdev.mercato.bg.repository.MainItemsRepository
import com.zerosdev.mercato.bg.viewmodel.ChatViewModel
import com.zerosdev.mercato.bg.viewmodel.MainItemViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class])
interface AppComponent {

    fun inject(chatViewModel: ChatViewModel)

    fun inject(mainItemViewModel: MainItemViewModel)

    fun inject(chatRepository: ChatRepository)

    fun inject(itemDetailRepository: ItemDetailRepository)

    fun inject(mainItemsRepository: MainItemsRepository)

    fun inject(mainItemDataSource: MainItemDataSource)

    fun inject(searchItemDataSource: SearchItemDataSource)

    fun inject(chatListItemDataSource: ChatListItemDataSource)

    fun inject(userItemDataSource: UserItemDataSource)



}