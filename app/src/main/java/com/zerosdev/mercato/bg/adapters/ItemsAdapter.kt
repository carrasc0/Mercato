package com.zerosdev.mercato.bg.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.util.inflate
import kotlinx.android.synthetic.main.item_design.view.*

class ItemsAdapter(var items: MutableList<Item>) : RecyclerView.Adapter<ItemsAdapter.ItemVH>() {

    fun addItems(items: MutableList<Item>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemVH =
        ItemVH(parent.inflate(R.layout.item_design, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ItemVH, position: Int) {
        holder.bind(items[position])
    }

    class ItemVH(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        fun bind(item: Item) {
            itemView.itemTitle.text = item.id.toString()
        }

        override fun onClick(view: View?) {
            //todo onclick items go to the item view
        }

    }
}