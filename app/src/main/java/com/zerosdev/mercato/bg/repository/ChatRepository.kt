package com.zerosdev.mercato.bg.repository

import com.zerosdev.mercato.bg.api.ChatInterface
import com.zerosdev.mercato.bg.base.BaseRepository
import com.zerosdev.mercato.bg.base.AppController
import com.zerosdev.mercato.bg.datamodel.Message

class ChatRepository(private val chatInterface: ChatInterface) : BaseRepository() {

    init {
        AppController.component.inject(this)
    }

    suspend fun getMessages(nickname: Int): MutableList<Message>? {
        return safeApiCall(
            call = {
                chatInterface.getMessages(nickname).await()
            },
            error = "Error fetching messages"
        )?.messages?.toMutableList()
    }

}