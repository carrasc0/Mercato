package com.zerosdev.mercato.bg.dagger.modules

import android.app.Application
import com.zerosdev.mercato.bg.api.ApiInterface
import com.zerosdev.mercato.bg.api.ChatInterface
import com.zerosdev.mercato.bg.persistence.Preferences
import com.zerosdev.mercato.bg.repository.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    internal fun provideApplication(): Application {
        return application
    }

    @Provides
    @Singleton
    internal fun provideChatRepository(chatInterface: ChatInterface): ChatRepository {
        return ChatRepository(chatInterface)
    }

    @Provides
    @Singleton
    internal fun provideMainRepository(apiInterface: ApiInterface): MainItemsRepository {
        return MainItemsRepository(apiInterface)
    }

    @Provides
    @Singleton
    internal fun provideSearchRepository(apiInterface: ApiInterface): SearchItemRepository {
        return SearchItemRepository(apiInterface)
    }

    @Provides
    @Singleton
    internal fun provideUserItemRepository(apiInterface: ApiInterface): UserItemsRepository {
        return UserItemsRepository(apiInterface)
    }

    @Provides
    @Singleton
    internal fun provideChatListRepository(apiInterface: ApiInterface): ChatListItemRepository {
        return ChatListItemRepository(apiInterface)
    }

    @Provides
    @Singleton
    internal fun providePreferences(): Preferences {
        return Preferences(application)
    }



    /*@Provides
    @Singleton
    internal fun provideChatRepository(chatInterface: ChatInterface): ChatRepository {
        return ChatRepository(chatInterface)
    }

    @Provides
    @Singleton
    internal fun provideDiscoverRepository(retrofitInterface: RetrofitInterface): DiscoverRepository {
        return DiscoverRepository(retrofitInterface)
    }

    @Provides
    @Singleton
    internal fun provideMatchRepository(retrofitInterface: RetrofitInterface): MatchRepository {
        return MatchRepository(retrofitInterface)
    }

    @Provides
    @Singleton
    internal fun provideMainActivityRepository(): MainActivityRepository {
        return MainActivityRepository()
    }*/

}