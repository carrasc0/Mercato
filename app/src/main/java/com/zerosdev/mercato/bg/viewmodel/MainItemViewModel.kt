package com.zerosdev.mercato.bg.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.zerosdev.mercato.bg.datasource.MainItemDataSource
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.util.TAG

class MainItemViewModel : BaseViewModel() {

    private var mainItemsLiveData: LiveData<PagedList<Item>>

    private val config = PagedList.Config.Builder()
        .setInitialLoadSizeHint(25)
        .setPageSize(15)
        .setEnablePlaceholders(false)
        .build()

    init {
        mainItemsLiveData = initializedPageListBuilder(config).build()
    }

    fun getMainItems(): LiveData<PagedList<Item>> = mainItemsLiveData

    private fun initializedPageListBuilder(config: PagedList.Config): LivePagedListBuilder<Int, Item> {
        Log.d(TAG(), "initializedPageListBuilder in MainItemViewModel")
        val dataSourceFactory = object : DataSource.Factory<Int, Item>() {
            override fun create(): DataSource<Int, Item> {
                return MainItemDataSource()
            }
        }
        return LivePagedListBuilder<Int, Item>(dataSourceFactory, config)
    }

}