package com.zerosdev.mercato.bg.datamodel.response

import com.zerosdev.mercato.bg.datamodel.Message

data class GetMessagesResponse(val messages: MutableList<Message>) : BaseResponse()
