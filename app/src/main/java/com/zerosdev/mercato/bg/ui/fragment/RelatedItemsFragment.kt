package com.zerosdev.mercato.bg.ui.fragment

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.adapters.ItemsAdapter
import com.zerosdev.mercato.bg.base.BaseFragment
import com.zerosdev.mercato.bg.factory.RelatedItemsViewModelFactory
import com.zerosdev.mercato.bg.util.Constant.Companion.CAT_ID

import com.zerosdev.mercato.bg.viewmodel.RelatedItemsViewModel
import kotlinx.android.synthetic.main.related_items_fragment.*

class RelatedItemsFragment : BaseFragment() {

    private lateinit var itemsAdapter: ItemsAdapter
    private var cat: Int = 1

    companion object {
        fun newInstance(cat: Int): RelatedItemsFragment {
            val args = Bundle()
            args.putInt(CAT_ID, cat)
            val fragment = RelatedItemsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var viewModel: RelatedItemsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cat = arguments?.getInt(CAT_ID) ?: 1
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.related_items_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            RelatedItemsViewModelFactory(cat = cat)
        )[RelatedItemsViewModel::class.java]
        viewModel.getRelatedItemsLiveData().observe(this, Observer {
            itemsAdapter.addItems(it.toMutableList())
        })
    }

    fun setupRecyclerView() {
        itemsAdapter = ItemsAdapter(mutableListOf())
        relatedItemsRecycler.apply {
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            adapter = itemsAdapter
            isNestedScrollingEnabled = false
        }
    }

}
