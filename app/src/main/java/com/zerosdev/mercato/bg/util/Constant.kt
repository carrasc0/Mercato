package com.zerosdev.mercato.bg.util

class Constant {

    companion object {
        //Intents
        const val DEP_ID = "dep_id"
        const val CAT_ID = "cat_id"
        const val USER_ID = "user_id"

        //Chat
        const val NEW_MESSAGE = "newMessage"
        const val TYPING = "typing"
        const val FUNCTION_KEY = "func"
        const val FN_KEY = "fn"
        const val PARAMS_KEY = "params"
    }



}