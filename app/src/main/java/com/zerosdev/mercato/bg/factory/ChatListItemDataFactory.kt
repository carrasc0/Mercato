package com.zerosdev.mercato.bg.factory

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.zerosdev.mercato.bg.datamodel.ChatListItem
import com.zerosdev.mercato.bg.datasource.ChatListItemDataSource

class ChatListItemDataFactory : DataSource.Factory<Int, ChatListItem>() {

    private lateinit var chatItemDataSource: ChatListItemDataSource
    private val mutableLiveData: MutableLiveData<ChatListItemDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, ChatListItem> {
        chatItemDataSource = ChatListItemDataSource()
        mutableLiveData.postValue(chatItemDataSource)
        return chatItemDataSource
    }

    fun getMutableLiveData(): MutableLiveData<ChatListItemDataSource> {
        return mutableLiveData
    }

}