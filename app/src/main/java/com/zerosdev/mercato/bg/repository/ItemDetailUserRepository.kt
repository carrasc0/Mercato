package com.zerosdev.mercato.bg.repository

import com.zerosdev.mercato.bg.api.ApiInterface
import com.zerosdev.mercato.bg.base.BaseRepository
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.datamodel.User

class ItemDetailUserRepository(private val apiInterface: ApiInterface) : BaseRepository() {

    suspend fun getUser(idUser: Int): User? {
        return safeApiCall(
            call = { apiInterface.getItemViewUser(idUser).await() },
            error = "Error fetching related messages"
        )?.user
    }

}