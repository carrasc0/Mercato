package com.zerosdev.mercato.bg.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import com.zerosdev.mercato.bg.base.AppController
import com.zerosdev.mercato.bg.datamodel.Message
import com.zerosdev.mercato.bg.datamodel.Typing
import com.zerosdev.mercato.bg.repository.ChatRepository
import kotlinx.coroutines.launch
import org.json.JSONObject
import javax.inject.Inject
import com.zerosdev.mercato.bg.util.Constant.Companion.FUNCTION_KEY
import com.zerosdev.mercato.bg.util.Constant.Companion.NEW_MESSAGE
import com.zerosdev.mercato.bg.util.Constant.Companion.TYPING
import com.zerosdev.mercato.bg.util.TAG

class ChatViewModel : BaseViewModel() {

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var chatRepository: ChatRepository

    @Inject
    lateinit var socket: Socket

    val messagesLiveData: MutableLiveData<MutableList<Message>> by lazy {
        MutableLiveData<MutableList<Message>>().also {
            getMessages()
        }
    }

    val typingLiveData: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>().also {
            it.postValue(false)
        }
    }

    init {
        AppController.component.inject(this)
    }

    private val onNewMessageListener: Emitter.Listener = Emitter.Listener { args ->
        processOnNewMessage(args[0] as JSONObject)
    }

    private val onTypingListener: Emitter.Listener = Emitter.Listener { args ->
        Log.d(TAG(), "onTypingEvent ${args[0]}")
        processOnTyping(args[0] as JSONObject)
    }

    private fun getMessages() {
        //launch the coroutine scope
        scope.launch {
            //get the messages
            val messages = chatRepository.getMessages(1)
            //post the value inside live data
            messagesLiveData.postValue(messages)
        }
    }

    fun sendMessage(message: Message) {
        scope.launch {
            val jsonMessage = gson.toJson(message)
            Log.d(TAG(), "message $message")
            socket.emit(FUNCTION_KEY, jsonMessage)
        }
    }

    fun sendTyping(typing: Typing) {
        scope.launch {
            val jsonTyping = gson.toJson(typing)
            Log.d(TAG(), "typing $typing")
            socket.emit(TYPING, jsonTyping)
        }
    }

    private fun processOnNewMessage(data: JSONObject) {
        val message = gson.fromJson(data.toString(), Message::class.java)
        if (!validateNewMessage(message)) {
            return
        } else {
            val newList = MutableList(1) { message }
            messagesLiveData.postValue(newList)
        }

    }

    private fun processOnTyping(data: JSONObject) {
        val typing = gson.fromJson(data.toString(), Typing::class.java)
        if (!validateTyping(typing)) return else typingLiveData.postValue(typing.typing)
    }

    private fun validateTyping(dataTyping: Typing): Boolean {
        return (1 == dataTyping.nickname) && (2 == dataTyping.sender)
    }

    private fun validateNewMessage(message: Message): Boolean {
        return ((1 == message.nickname) && (2 == message.sender) ||
                1 == message.sender && 2 == message.nickname)
    }

    fun connectSocket() {
        socket.connect()
        socket.on(NEW_MESSAGE, onNewMessageListener)
        socket.on(TYPING, onTypingListener)
    }

    fun disconnectSocket() {
        socket.off(NEW_MESSAGE)
        socket.off(TYPING)
        socket.disconnect()
    }

}