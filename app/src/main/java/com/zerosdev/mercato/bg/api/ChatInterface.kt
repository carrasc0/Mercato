package com.zerosdev.mercato.bg.api

import com.zerosdev.mercato.bg.datamodel.response.GetMessagesResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ChatInterface {

    @Headers("Content-Type: application/json")
    @POST("/getMessages")
    fun getMessages(@Body request: Int): Deferred<Response<GetMessagesResponse>>

    @Headers("Content-Type: application/json")
    @POST("/getMessages")
    fun getMoreMessages(@Body request: Int): Deferred<Int>

}