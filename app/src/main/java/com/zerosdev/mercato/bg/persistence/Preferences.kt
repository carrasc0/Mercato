package com.zerosdev.mercato.bg.persistence

import android.content.Context
import android.content.SharedPreferences
import com.zerosdev.mercato.bg.projectmodel.Location
class Preferences(context: Context) {

    private val sharedPreferences: SharedPreferences
    private val editor: SharedPreferences.Editor

    init {
        sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }

    fun setCurrentLocation(location: Location) {
        editor.putFloat(POSITION_LAT, location.lat)
        editor.putFloat(POSITION_lONG, location.long)
        editor.commit()
    }

    fun setUserId(id: Int) {
        editor.putInt(USER_ID, id)
        editor.commit()
    }

    fun setUserName(name: String) {
        editor.putString(USER_NAME, name)
        editor.commit()
    }

    fun getUserId(): Int {
        return sharedPreferences.getInt(USER_ID, 1)
    }

    fun getUserName(): String? {
        return sharedPreferences.getString(USER_NAME, "")
    }

    fun getCurrentLocation(): Location? {
        return Location(
            sharedPreferences.getFloat(POSITION_LAT, 0f),
            sharedPreferences.getFloat(POSITION_lONG, 0f)
        )
    }

    companion object {
        private const val SHARED_PREFERENCES_NAME = "MercatoPreferences"
        private const val USER_ID = "id"
        private const val USER_NAME = "nameUser"
        private const val POSITION_LAT = "lat"
        private const val POSITION_lONG = "long"
    }

}