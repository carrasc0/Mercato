package com.zerosdev.mercato.bg.interfaces

import com.zerosdev.mercato.bg.projectmodel.SearchTag

interface OnSearchTagSelected {
    fun onTagSelected(searchTag: SearchTag)
}