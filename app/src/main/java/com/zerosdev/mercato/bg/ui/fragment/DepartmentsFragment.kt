package com.zerosdev.mercato.bg.ui.fragment

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.adapters.DepartmentsAdapter
import com.zerosdev.mercato.bg.base.BaseFragment
import com.zerosdev.mercato.bg.viewmodel.DepartmentsViewModel
import kotlinx.android.synthetic.main.departments_fragment.*


class DepartmentsFragment : BaseFragment() {

    companion object {
        fun newInstance() = DepartmentsFragment()
    }

    private lateinit var viewModel: DepartmentsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.departments_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DepartmentsViewModel::class.java)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        departmentsRecyclerView.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        departmentsRecyclerView.adapter =
            DepartmentsAdapter(viewModel.departments) {
                Toast.makeText(requireContext(), "department: ${it.name}", Toast.LENGTH_SHORT)
                    .show()
            }
    }

}
