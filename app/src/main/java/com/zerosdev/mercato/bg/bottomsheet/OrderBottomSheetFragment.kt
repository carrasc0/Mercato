package com.zerosdev.mercato.bg.bottomsheet

import android.content.Context
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.projectmodel.Order
import kotlinx.android.synthetic.main.fragment_order_list_dialog.*
import kotlinx.android.synthetic.main.fragment_order_list_dialog_item.view.*

/**
 *
 * A fragment that shows a list of items as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 *    OrderBottomSheetFragment.newInstance(30).show(supportFragmentManager, "dialog")
 * </pre>
 *
 * You activity (or fragment) needs to implement [OrderBottomSheetFragment.Listener].
 */
class OrderBottomSheetFragment : BottomSheetDialogFragment() {
    private var mListener: Listener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_order_list_dialog, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        list.layoutManager = LinearLayoutManager(context)
        list.adapter = OrderAdapter()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        if (parent != null) {
            mListener = parent as Listener
        } else {
            mListener = context as Listener
        }
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
    }

    interface Listener {
        fun onOrderClicked(order: Order)
    }

    private inner class OrderAdapter internal constructor
        (private val orders: Array<Order> = Order.values()) :
        RecyclerView.Adapter<OrderAdapter.OrderVH>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderVH {
            return OrderVH(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: OrderVH, position: Int) {
            holder.bind(orders[position])
        }

        override fun getItemCount() = orders.size

        private inner class OrderVH internal constructor(
            inflater: LayoutInflater,
            parent: ViewGroup
        ) : RecyclerView.ViewHolder(
            inflater.inflate(
                R.layout.fragment_order_list_dialog_item,
                parent,
                false
            )
        ) {

            internal val text: TextView = itemView.text

            init {
                text.setOnClickListener {
                    mListener?.let {
                        it.onOrderClicked(orders[adapterPosition])
                        dismiss()
                    }
                }
            }

            fun bind(order: Order) {
                text.text = itemView.context.getString(order.stringRes)
            }
        }
    }


    companion object {
        fun newInstance(): OrderBottomSheetFragment =
            OrderBottomSheetFragment()
    }

}
