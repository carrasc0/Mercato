package com.zerosdev.mercato.bg.projectmodel

import com.zerosdev.mercato.bg.R

enum class Order(val id: Int, val stringRes: Int) {
    DISTANCE(1, R.string.order_distance),
    CHEAP_EXP(2, R.string.order_cheap_exp),
    EXP_CHEAP(3, R.string.order_exp_cheap)
}