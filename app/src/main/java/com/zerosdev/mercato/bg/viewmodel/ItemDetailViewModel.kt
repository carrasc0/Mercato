package com.zerosdev.mercato.bg.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.zerosdev.mercato.bg.datamodel.Comment
import com.zerosdev.mercato.bg.datamodel.Item

class ItemDetailViewModel : BaseViewModel() {

    private val commentsLiveData: MutableLiveData<List<Comment>> = MutableLiveData()
    private val itemLiveData: MutableLiveData<Item> = MutableLiveData()

    fun getComments(): LiveData<List<Comment>> = commentsLiveData
    fun getItem(): LiveData<Item> = itemLiveData



}