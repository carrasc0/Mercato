package com.zerosdev.mercato.bg.ui.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.adapters.PagingItemsAdapter
import com.zerosdev.mercato.bg.viewmodel.SearchItemViewModel
import kotlinx.android.synthetic.main.fragment_search_item_list.*

class SearchItemListFragment : Fragment() {

    private val itemsAdapter = PagingItemsAdapter()
    lateinit var searchItemViewModel: SearchItemViewModel

    companion object {
        fun newInstance() = SearchItemListFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_item_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewModel()
    }


    private fun setupViewModel() {
        searchItemViewModel = ViewModelProviders.of(this)[SearchItemViewModel::class.java]
        searchItemViewModel.getSearchItems().observe(this, Observer {
            itemsAdapter.submitList(it)
        })
    }

    fun setupRecyclerView() {
        searchItemRecyclerView.apply {
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            adapter = itemsAdapter
            isNestedScrollingEnabled = false
        }
    }

}
