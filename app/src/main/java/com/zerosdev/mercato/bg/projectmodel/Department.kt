package com.zerosdev.mercato.bg.projectmodel

import android.os.Parcelable
import com.zerosdev.mercato.bg.R
import kotlinx.android.parcel.Parcelize

sealed class Department {
    val name: Int
        get() = when (this) {
            is Cars -> R.string.cars
            is Motos -> R.string.motos
            is ModaAccesorios -> R.string.moda
            is Inmobiliaria -> R.string.inmobiliaria
            is MovilesAccesorios -> R.string.moviles
            is TvAudioFoto -> R.string.tv_audio
            is ComputadorasAccesorios -> R.string.computadoras
            is DeporteOcio -> R.string.deporte
            is ConsolasVideojuegos -> R.string.consolas
            is HogarJardin -> R.string.hogar
            is Electrodomesticos -> R.string.electrodomesticos
            is NinosBebes -> R.string.ninnos
            is CineLibrosMusica -> R.string.cine_libros
            is Coleccionismo -> R.string.coleccionismo
            is Otros -> R.string.otros
        }

    val image: Int
        get() = when (this) {
            else -> R.drawable.house
        }

    val hasCategories: Boolean
        get() = when (this) {
            is Inmobiliaria -> true
            is ModaAccesorios -> true
            else -> false
        }

    val serverId: Int
        get() = when (this) {
            is Cars -> TODO()
            is Motos -> TODO()
            is ModaAccesorios -> TODO()
            is Inmobiliaria -> TODO()
            is MovilesAccesorios -> TODO()
            is TvAudioFoto -> TODO()
            is ComputadorasAccesorios -> TODO()
            is DeporteOcio -> TODO()
            is ConsolasVideojuegos -> TODO()
            is HogarJardin -> TODO()
            is Electrodomesticos -> TODO()
            is NinosBebes -> TODO()
            is CineLibrosMusica -> TODO()
            is Coleccionismo -> TODO()
            is Otros -> TODO()
        }

    companion object {
        fun getAllDepartments(): ArrayList<Department> {
            return arrayListOf(
                Cars(),
                Motos(),
                ModaAccesorios(),
                Inmobiliaria(),
                MovilesAccesorios(),
                TvAudioFoto(),
                ComputadorasAccesorios(),
                DeporteOcio(),
                ConsolasVideojuegos(),
                HogarJardin(),
                Electrodomesticos(),
                NinosBebes(),
                CineLibrosMusica(),
                Coleccionismo(),
                Otros()
            )
        }
        fun getCountDepartment(): Int{
            return getAllDepartments().size
        }
    }

}

class Cars : Department()
class Motos() : Department()
class ModaAccesorios() : Department() {
    sealed class ModaCategories {

        val name: Int
            get() = when (this) {
                is Mujeres -> TODO()
                is Hombres -> TODO()
            }

        val serverId: Int
            get() = when (this) {
                is Mujeres -> TODO()
                is Hombres -> TODO()
            }

        class Mujeres : ModaCategories()
        class Hombres : ModaCategories()
    }
}
class Inmobiliaria : Department() {
    sealed class InmobiliariaCategories {

        val name: Int
            get() = when (this) {
                is Renta -> TODO()
                is Venta -> TODO()
            }

        class Renta : InmobiliariaCategories() {
            sealed class RentaCategories {

            }
        }

        class Venta : InmobiliariaCategories() {
            sealed class VentaCategories {

            }
        }
    }
}
class MovilesAccesorios : Department()
class TvAudioFoto : Department()
class ComputadorasAccesorios : Department()
class DeporteOcio : Department()
class ConsolasVideojuegos : Department()
class HogarJardin : Department()
class Electrodomesticos : Department()
class NinosBebes : Department()
class CineLibrosMusica : Department()
class Coleccionismo : Department()
class Otros : Department()



