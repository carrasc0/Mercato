package com.zerosdev.mercato.bg.datamodel.response

import com.zerosdev.mercato.bg.datamodel.ChatListItem

data class GetChatListItemResponse(val items: MutableList<ChatListItem>) : BaseResponse()
