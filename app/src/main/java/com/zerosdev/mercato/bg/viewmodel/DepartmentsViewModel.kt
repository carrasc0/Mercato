package com.zerosdev.mercato.bg.viewmodel

import com.zerosdev.mercato.bg.projectmodel.Department

class DepartmentsViewModel : BaseViewModel() {

    val departments: ArrayList<Department> = Department.getAllDepartments()

}
