package com.zerosdev.mercato.bg.projectmodel

import com.zerosdev.mercato.bg.R

enum class SearchTag(val icon: Int, val stringRes: Int, val actionIcon: Int) {
    DEPARTMENT(
        R.drawable.ic_filter_list_black_24dp,
        R.string.tag_department,
        R.drawable.ic_expand_more_black_24dp
    ),
    PRICE(
        R.drawable.ic_filter_list_black_24dp,
        R.string.tag_price,
        R.drawable.ic_expand_more_black_24dp
    ),
    ORDER(
        R.drawable.ic_filter_list_black_24dp,
        R.string.tag_order,
        R.drawable.ic_expand_more_black_24dp
    )
}