package com.zerosdev.mercato.bg.repository

import com.zerosdev.mercato.bg.api.ApiInterface
import com.zerosdev.mercato.bg.base.BaseRepository
import com.zerosdev.mercato.bg.datamodel.Item

class UserItemsRepository(private val apiInterface: ApiInterface) : BaseRepository() {

    suspend fun getUserItems(page: Int, idUser: Int): MutableList<Item>? {
        return safeApiCall(
            call = { apiInterface.getUserItems(page = page, idUser = idUser).await() },
            error = "Error fetching main messages"
        )?.items?.toMutableList()
    }

}