package com.zerosdev.mercato.bg.datamodel.response

import com.zerosdev.mercato.bg.datamodel.Item

data class GetUserItemsResponse(val items: MutableList<Item>) : BaseResponse()
