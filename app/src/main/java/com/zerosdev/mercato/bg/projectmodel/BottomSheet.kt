package com.zerosdev.mercato.bg.projectmodel

enum class BottomSheet {
    DEPARTMENT, PRICE, ORDER
}