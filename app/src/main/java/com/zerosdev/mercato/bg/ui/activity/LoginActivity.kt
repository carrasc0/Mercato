package com.zerosdev.mercato.bg.ui.activity

import android.os.Bundle
import com.zerosdev.mercato.bg.base.BaseActivity
import com.zerosdev.mercato.bg.R

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }
}
