package com.zerosdev.mercato.bg.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.adapters.ChatListAdapter
import com.zerosdev.mercato.bg.viewmodel.ChatListItemViewModel

import kotlinx.android.synthetic.main.activity_chat_list.*
import kotlinx.android.synthetic.main.content_chat_list.*

class ChatListItemActivity : AppCompatActivity() {

    private lateinit var chatListItemViewModel: ChatListItemViewModel
    private lateinit var chatListAdapter: ChatListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_list)
        setSupportActionBar(toolbar)
        setupViews()
        setupViewModel()
    }

    private fun setupViews() {
        chatListAdapter = ChatListAdapter()
        chatListItemRecycler.apply {
            layoutManager = LinearLayoutManager(this@ChatListItemActivity)
            adapter = chatListAdapter
            setHasFixedSize(true)
        }
    }

    private fun setupViewModel() {
        chatListItemViewModel =
            ViewModelProviders.of(this)[ChatListItemViewModel::class.java].apply {
                getChatItems().observe(this@ChatListItemActivity, Observer {
                    chatListAdapter.submitList(it)
                })
            }

    }

}
