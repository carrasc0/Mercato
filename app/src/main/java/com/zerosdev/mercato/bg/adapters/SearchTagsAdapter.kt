package com.zerosdev.mercato.bg.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.projectmodel.SearchTag
import com.zerosdev.mercato.bg.util.inflate
import kotlinx.android.synthetic.main.search_tag_design.view.*

class SearchTagsAdapter(
    private val tags: Array<SearchTag> = SearchTag.values(),
    private val listener: (SearchTag) -> Unit
) :
    RecyclerView.Adapter<SearchTagsAdapter.TagVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TagVH(parent.inflate(R.layout.search_tag_design))

    override fun getItemCount() = tags.size

    override fun onBindViewHolder(holder: TagVH, position: Int) =
        holder.bindView(tags[position], listener)

    inner class TagVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(searchTag: SearchTag, listener: (SearchTag) -> Unit) = with(itemView) {
            itemView.searchTagTextView.text = itemView.context.getString(searchTag.stringRes)
            //todo ubicar drawables as well
            setOnClickListener { listener(searchTag) }
        }

    }

}