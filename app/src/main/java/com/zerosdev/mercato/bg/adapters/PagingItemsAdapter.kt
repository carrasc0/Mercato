package com.zerosdev.mercato.bg.adapters

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.util.inflate
import kotlinx.android.synthetic.main.item_design.view.*

class PagingItemsAdapter : PagedListAdapter<Item, PagingItemsAdapter.ItemVH>(ItemComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemVH {
        return ItemVH(parent.inflate(R.layout.item_design))
    }

    override fun onBindViewHolder(holder: ItemVH, position: Int) {
        val item = getItem(position)
        item?.let { holder.bind(it) }
    }


    class ItemVH(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        fun bind(item: Item) {
            itemView.itemTitle.text = item.id.toString()
        }

        override fun onClick(view: View?) {
            //todo onclick items go to the item view
        }

    }

    companion object {
        private val ItemComparator = object : DiffUtil.ItemCallback<Item>() {
            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean =
                newItem == oldItem
        }
    }

}