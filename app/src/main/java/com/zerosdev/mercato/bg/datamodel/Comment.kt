package com.zerosdev.mercato.bg.datamodel

import com.google.gson.annotations.SerializedName

data class Comment(
    val id: Int,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("user_name")
    val userName: String,
    @SerializedName("user_image")
    val userImage: String,
    val body: String
)