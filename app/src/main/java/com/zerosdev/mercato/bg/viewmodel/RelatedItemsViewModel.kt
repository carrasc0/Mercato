package com.zerosdev.mercato.bg.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.repository.RelatedItemsRepository
import kotlinx.coroutines.launch
import javax.inject.Inject


class RelatedItemsViewModel(private val cat: Int) : BaseViewModel() {

    @Inject
    lateinit var relatedItemsRepository: RelatedItemsRepository

    private val relatedItemsLiveData: MutableLiveData<List<Item>> by lazy {
        MutableLiveData<List<Item>>().also {
            getRelatedItems(cat)
        }
    }

    fun getRelatedItemsLiveData(): LiveData<List<Item>> = relatedItemsLiveData

    fun getRelatedItems(cat: Int) {
        scope.launch {
            relatedItemsLiveData.postValue(relatedItemsRepository.getRelatedItems(cat))
        }
    }


}
