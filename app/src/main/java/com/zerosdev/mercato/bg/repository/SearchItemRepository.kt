package com.zerosdev.mercato.bg.repository

import com.zerosdev.mercato.bg.api.ApiInterface
import com.zerosdev.mercato.bg.base.BaseRepository
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.projectmodel.Order
import com.zerosdev.mercato.bg.projectmodel.Price

class SearchItemRepository(private val apiInterface: ApiInterface) : BaseRepository() {

    suspend fun getSearchItems(
        page: Int,
        dep: Int,
        price: Price,
        order: Order = Order.CHEAP_EXP
    ): MutableList<Item>? {
        return safeApiCall(
            call = {
                apiInterface.getSearchItems(
                    page = page, dep = dep, min = price.min ?: 0,
                    max = price.max ?: 0, order = order.id
                ).await()
            },
            error = "Error fetching search messages"
        )?.items?.toMutableList()
    }

}