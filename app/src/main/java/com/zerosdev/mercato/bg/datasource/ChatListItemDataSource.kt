package com.zerosdev.mercato.bg.datasource

import androidx.paging.PageKeyedDataSource
import com.zerosdev.mercato.bg.base.AppController
import com.zerosdev.mercato.bg.datamodel.ChatListItem
import com.zerosdev.mercato.bg.repository.ChatListItemRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

class ChatListItemDataSource : PageKeyedDataSource<Int, ChatListItem>() {

    @Inject
    lateinit var chatItemsRepository: ChatListItemRepository

    init {
        AppController.component.inject(this)
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, ChatListItem>
    ) {
        GlobalScope.launch {
            try {
                chatItemsRepository.getChatListItems(1)?.let {
                    callback.onResult(it, null, 2)
                }
            } catch (exception: IOException) {
                exception.printStackTrace()
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, ChatListItem>) {
        GlobalScope.launch {
            try {
                chatItemsRepository.getChatListItems(page = params.key)?.let {
                    callback.onResult(it, params.key + 1)
                }
            } catch (exception: IOException) {
                exception.printStackTrace()
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, ChatListItem>) {
        GlobalScope.launch {
            try {
                chatItemsRepository.getChatListItems(page = params.key)?.let {
                    callback.onResult(it, params.key - 1)
                }
            } catch (exception: IOException) {
                exception.printStackTrace()
            }
        }
    }
}