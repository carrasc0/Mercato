package com.zerosdev.mercato.bg.datamodel

data class Typing(val sender: Int, val nickname: Int, val typing: Boolean)