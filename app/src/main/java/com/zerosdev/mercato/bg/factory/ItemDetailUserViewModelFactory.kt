package com.zerosdev.mercato.bg.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ItemDetailUserViewModelFactory(private val idUser: Int) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ItemDetailUserViewModelFactory(idUser) as T
    }

}