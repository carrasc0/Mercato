package com.zerosdev.mercato.bg.repository

import com.zerosdev.mercato.bg.api.ApiInterface
import com.zerosdev.mercato.bg.base.BaseRepository
import com.zerosdev.mercato.bg.base.AppController
import com.zerosdev.mercato.bg.datamodel.Comment

class ItemDetailRepository(private val apiInterface: ApiInterface) : BaseRepository() {

    init {
        AppController.component.inject(this)
    }

    suspend fun getComments(idItem: Int): MutableList<Comment>? {
        return safeApiCall(
            call = {
                apiInterface.getComments(idItem = idItem).await()
            },
            error = "Error fetching messages"
        )?.comments?.toMutableList()
    }

}