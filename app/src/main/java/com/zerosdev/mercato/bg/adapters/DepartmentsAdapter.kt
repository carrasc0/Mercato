package com.zerosdev.mercato.bg.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.projectmodel.Department
import com.zerosdev.mercato.bg.util.inflate
import kotlinx.android.synthetic.main.department_item_design.view.*

class DepartmentsAdapter(
    private val items: ArrayList<Department>,
    private val listener: (Department) -> Unit
) :
    RecyclerView.Adapter<DepartmentsAdapter.DepartmentVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DepartmentVH =
        DepartmentVH(parent.inflate(R.layout.department_item_design, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: DepartmentVH, position: Int) =
        holder.bindView(items[position], listener)

    inner class DepartmentVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(department: Department, listener: (Department) -> Unit) = with(itemView) {
            itemView.departmentIcon.setImageResource(department.image)
            itemView.departmentText.text = itemView.context.getString(department.name)
            setOnClickListener { listener(department) }
        }

    }
}