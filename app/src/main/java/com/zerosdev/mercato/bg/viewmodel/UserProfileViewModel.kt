package com.zerosdev.mercato.bg.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.datamodel.User
import com.zerosdev.mercato.bg.datasource.UserItemDataSource
import com.zerosdev.mercato.bg.util.TAG

class UserProfileViewModel : BaseViewModel() {

    private var userItemsLiveData: LiveData<PagedList<Item>>
    private var userLiveData: MutableLiveData<User>

    private val config = PagedList.Config.Builder()
        .setInitialLoadSizeHint(25)
        .setPageSize(15)
        .setEnablePlaceholders(false)
        .build()

    init {
        userItemsLiveData = initializedPageListBuilder(config).build()
        userLiveData = MutableLiveData()
    }

    fun getUserItems(): LiveData<PagedList<Item>> = userItemsLiveData

    fun getUser(): LiveData<User> = userLiveData

    private fun initializedPageListBuilder(config: PagedList.Config): LivePagedListBuilder<Int, Item> {
        Log.d(TAG(), "initializedPageListBuilder in UserProfileViewModel")
        val dataSourceFactory = object : DataSource.Factory<Int, Item>() {
            override fun create(): DataSource<Int, Item> {
                return UserItemDataSource()
            }
        }
        return LivePagedListBuilder<Int, Item>(dataSourceFactory, config)
    }

}