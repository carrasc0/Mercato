package com.zerosdev.mercato.bg.projectmodel

data class Location(val lat: Float, val long: Float)