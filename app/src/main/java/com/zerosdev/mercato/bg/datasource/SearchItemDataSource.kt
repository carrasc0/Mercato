package com.zerosdev.mercato.bg.datasource

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.zerosdev.mercato.bg.base.AppController
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.projectmodel.Order
import com.zerosdev.mercato.bg.projectmodel.Price
import com.zerosdev.mercato.bg.repository.SearchItemRepository
import com.zerosdev.mercato.bg.util.TAG
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject


class SearchItemDataSource(
    private val dep: Int,
    private val price: Price,
    private val order: Order
) :
    PageKeyedDataSource<Int, Item>() {

    @Inject
    lateinit var searchItemRepository: SearchItemRepository

    init {
        AppController.component.inject(this)
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Item>
    ) {
        GlobalScope.launch {
            try {
                val items = searchItemRepository.getSearchItems(
                    page = 1,
                    dep = dep,
                    price = price,
                    order = order
                )
                callback.onResult(items!!, null, 2)
            } catch (exception: IOException) {
                println(exception)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        GlobalScope.launch {
            try {
                val items = searchItemRepository.getSearchItems(
                    page = params.key, dep = dep,
                    price = price,
                    order = order
                )
                callback.onResult(items!!, params.key + 1)
            } catch (exception: IOException) {
                println(exception)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        GlobalScope.launch {
            try {
                searchItemRepository.getSearchItems(
                    page = params.key, dep = dep,
                    price = price,
                    order = order
                )?.let {
                    callback.onResult(it, params.key - 1).also {
                        Log.d(
                            TAG(),
                            "load before $it ${params.requestedLoadSize} ${params.key}"
                        )
                    }
                }
            } catch (exception: IOException) {
                println(exception)
            }
        }
    }


}