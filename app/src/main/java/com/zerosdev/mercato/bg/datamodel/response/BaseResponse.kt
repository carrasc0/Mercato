package com.zerosdev.mercato.bg.datamodel.response

open class BaseResponse {

    val success: Boolean = false
    val message: String? = null

}
