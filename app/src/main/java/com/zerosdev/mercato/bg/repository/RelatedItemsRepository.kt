package com.zerosdev.mercato.bg.repository

import com.zerosdev.mercato.bg.api.ApiInterface
import com.zerosdev.mercato.bg.base.BaseRepository
import com.zerosdev.mercato.bg.datamodel.Item

class RelatedItemsRepository(private val apiInterface: ApiInterface) : BaseRepository() {

    suspend fun getRelatedItems(cat: Int): MutableList<Item>? {
        return safeApiCall(
            call = { apiInterface.getRelatedItems(cat).await() },
            error = "Error fetching related messages"
        )?.items?.toMutableList()
    }

}