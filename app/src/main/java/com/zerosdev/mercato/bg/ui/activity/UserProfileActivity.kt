package com.zerosdev.mercato.bg.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.adapters.PagingItemsAdapter
import com.zerosdev.mercato.bg.viewmodel.UserProfileViewModel
import kotlinx.android.synthetic.main.activity_user_profile.*
import kotlinx.android.synthetic.main.content_user_profile.*

class UserProfileActivity : AppCompatActivity() {

    private val itemsAdapter = PagingItemsAdapter()
    private lateinit var userProfileViewModel: UserProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)
        setSupportActionBar(toolbar)

        setupRecyclerView()
        setupViewModel()

    }

    private fun setupViewModel() {
        userProfileViewModel = ViewModelProviders.of(this)[UserProfileViewModel::class.java]
        userProfileViewModel.getUserItems().observe(this, Observer {
            itemsAdapter.submitList(it)
        })
        userProfileViewModel.getUser().observe(this, Observer {
            it.apply {
                //todo ubicar user
            }
        })
    }

    fun setupRecyclerView() {
        profileUserRecycler.apply {
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            adapter = itemsAdapter
            isNestedScrollingEnabled = false
        }
    }

}
