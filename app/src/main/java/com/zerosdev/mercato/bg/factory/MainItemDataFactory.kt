package com.zerosdev.mercato.bg.factory

import androidx.arch.core.util.Function
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.zerosdev.mercato.bg.datasource.MainItemDataSource
import com.zerosdev.mercato.bg.datamodel.Item

class MainItemDataFactory: DataSource.Factory<Int, Item>() {

    private lateinit var mainItemDataSource: MainItemDataSource
    private val mutableLiveItemData: MutableLiveData<MainItemDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, Item> {
        mainItemDataSource = MainItemDataSource()
        mutableLiveItemData.postValue(mainItemDataSource)
        return mainItemDataSource
    }

    override fun <ToValue : Any?> map(function: Function<Item, ToValue>): DataSource.Factory<Int, ToValue> {
        return super.map(function)
    }

    override fun <ToValue : Any?> mapByPage(function: Function<MutableList<Item>, MutableList<ToValue>>): DataSource.Factory<Int, ToValue> {
        return super.mapByPage(function)
    }

    fun getMutableLiveData(): MutableLiveData<MainItemDataSource> {
        return mutableLiveItemData
    }




}