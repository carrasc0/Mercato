package com.zerosdev.mercato.bg.ui.activity

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.bottomsheet.DepartmentBottomSheetFragment
import com.zerosdev.mercato.bg.bottomsheet.OrderBottomSheetFragment
import com.zerosdev.mercato.bg.custom.SearchToolbar
import com.zerosdev.mercato.bg.projectmodel.Department
import com.zerosdev.mercato.bg.projectmodel.Order
import com.zerosdev.mercato.bg.util.TAG
import com.zerosdev.mercato.bg.viewmodel.SearchItemViewModel

import kotlinx.android.synthetic.main.activity_search_item.*
import kotlinx.android.synthetic.main.search_toolbar.view.*

class SearchItemActivity : AppCompatActivity(), DepartmentBottomSheetFragment.Listener,
    OrderBottomSheetFragment.Listener {

    private lateinit var searchItemViewModel: SearchItemViewModel
    private lateinit var searchToolbar: SearchToolbar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_search_item)
        setSupportActionBar(toolbar)

        setupViews()
        setupViewModel()

    }

    private fun setupViews() {
        searchToolbar = findViewById(R.id.searchToolbar)
        searchToolbar.backImageButton.setOnClickListener { view ->
            Log.d(TAG(), "back pressed")
        }
        searchToolbar.searchImageButton.setOnClickListener { view ->
            Log.d(TAG(), "search pressed")
        }
        searchToolbar.filterImageButton.setOnClickListener { view ->
            Log.d(TAG(), "filter pressed")
        }
    }

    private fun setupViewModel() {
        searchItemViewModel = ViewModelProviders.of(this)[SearchItemViewModel::class.java].apply {
            getSearchItems().observe(this@SearchItemActivity, Observer {
                //todo() submit list here to the adapter
            })
        }
    }

    override fun onDepartmentClicked(department: Department) {
        Log.d(TAG(), "department clicked ${department.name}")
        Toast.makeText(this, department.name, Toast.LENGTH_SHORT).show()
        searchItemViewModel.updateDepartment(department)
    }

    override fun onOrderClicked(order: Order) {
        Log.d(TAG(), "order clicked ${order.stringRes}")
        Toast.makeText(this, order.name, Toast.LENGTH_SHORT).show()
        searchItemViewModel.updateOrder(order)
    }

}
