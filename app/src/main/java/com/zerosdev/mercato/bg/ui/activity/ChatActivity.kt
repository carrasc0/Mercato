package com.zerosdev.mercato.bg.ui.activity

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.base.BaseActivity
import com.zerosdev.mercato.bg.datamodel.Message
import com.zerosdev.mercato.bg.datamodel.Typing
import com.zerosdev.mercato.bg.util.TAG
import com.zerosdev.mercato.bg.viewmodel.ChatViewModel

import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : BaseActivity(), View.OnClickListener {

    lateinit var viewModel: ChatViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        setSupportActionBar(toolbar)
        initViewModel()

    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this)[ChatViewModel::class.java]
        viewModel.messagesLiveData.observe(this, Observer { messages ->
            //update UI
        })
        viewModel.typingLiveData.observe(this, Observer { typing ->
            //update typing
        })
        viewModel.connectSocket()
    }

    private fun initListeners() {

    }

    private fun performTyping(typing: Boolean) {
        Log.d(TAG(), "performTyping $typing")
        //if (typing) chatLinearLayout.visibility = View.VISIBLE else chatLinearLayout.visibility = View.GONE
    }

    private fun sendMessage() {
        sendTyping(false)
        //val text = sendEditText!!.text.toString().trim { it <= ' ' }
        if (TextUtils.isEmpty("")) {
            return
        }
        viewModel.sendMessage(Message(1, 2, " "))
        //sendEditText.setText("")
    }

    private fun sendTyping(typing: Boolean) {
        viewModel.sendTyping(Typing(1, 2, typing))
    }

    override fun onClick(view: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStop() {
        super.onStop()
        viewModel.disconnectSocket()
    }
}
