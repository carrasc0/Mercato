package com.zerosdev.mercato.bg.datasource

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.zerosdev.mercato.bg.base.AppController
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.repository.MainItemsRepository
import com.zerosdev.mercato.bg.util.TAG
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

class MainItemDataSource : PageKeyedDataSource<Int, Item>() {

    @Inject
    lateinit var mainItemsRepository: MainItemsRepository

    init {
        AppController.component.inject(this)
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Item>
    ) {
        GlobalScope.launch {
            try {
                val items = mainItemsRepository.getMainItems(page = 1)
                callback.onResult(items!!, null, 2)
            } catch (exception: IOException) {
                println(exception)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        GlobalScope.launch {
            try {
                val items = mainItemsRepository.getMainItems(page = params.key)
                callback.onResult(items!!, params.key + 1)
            } catch (exception: IOException) {
                println(exception)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Item>) {
        GlobalScope.launch {
            try {
                mainItemsRepository.getMainItems(page = params.key)?.let {
                    callback.onResult(it, params.key - 1).also {
                        Log.d(
                            TAG(),
                            "load before $it ${params.requestedLoadSize} ${params.key}"
                        )
                    }
                }
            } catch (exception: IOException) {
                println(exception)
            }
        }
    }


}