package com.zerosdev.mercato.bg.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.zerosdev.mercato.bg.projectmodel.Department
import com.zerosdev.mercato.bg.datasource.SearchItemDataSource
import com.zerosdev.mercato.bg.datamodel.Item
import com.zerosdev.mercato.bg.projectmodel.Order
import com.zerosdev.mercato.bg.projectmodel.Price
import com.zerosdev.mercato.bg.util.TAG

class SearchItemViewModel : BaseViewModel() {

    private var searchItemLiveData: LiveData<PagedList<Item>>

    private val department: MutableLiveData<Department> = MutableLiveData()

    private val price: MutableLiveData<Price> = MutableLiveData()

    private val order: MutableLiveData<Order> = MutableLiveData()

    private val config = PagedList.Config.Builder()
        .setInitialLoadSizeHint(25)
        .setPageSize(15)
        .setEnablePlaceholders(false)
        .build()

    init {
        searchItemLiveData = initializedPageListBuilder(config).build()
    }

    fun getDepartment(): LiveData<Department> = department

    fun getPrice(): LiveData<Price> = price

    fun getOrder(): LiveData<Order> = order

    fun getSearchItems(): LiveData<PagedList<Item>> = searchItemLiveData

    fun updateDepartment(department: Department) {
        this.department.postValue(department)
        //todo check creo q esto reinicia el proceso
        searchItemLiveData = initializedPageListBuilder(config).build()
    }

    fun updatePrice(price: Price) {
        this.price.postValue(price)
        //todo check creo q esto reinicia el proceso
        searchItemLiveData = initializedPageListBuilder(config).build()
    }

    fun updateOrder(order: Order) {
        this.order.postValue(order)
        //todo check creo q esto reinicia el proceso
        searchItemLiveData = initializedPageListBuilder(config).build()
    }

    fun initializedPageListBuilder(config: PagedList.Config): LivePagedListBuilder<Int, Item> {
        Log.d(
            TAG(), "initializedPageListBuilder in SearchItemViewModel dep: ${getDepartment()}" +
                    " price: ${getPrice()} order: ${getOrder()}"
        )
        val dataSourceFactory = object : DataSource.Factory<Int, Item>() {
            override fun create(): DataSource<Int, Item> {
                return SearchItemDataSource(
                    getDepartment().value?.serverId ?: 1,
                    getPrice().value ?: Price(0, 0),
                    getOrder().value ?: Order.DISTANCE
                )
            }
        }
        return LivePagedListBuilder<Int, Item>(dataSourceFactory, config)
    }

}