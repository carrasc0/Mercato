package com.zerosdev.mercato.bg.ui.fragment

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.datamodel.User
import com.zerosdev.mercato.bg.factory.ItemDetailUserViewModelFactory
import com.zerosdev.mercato.bg.util.Constant.Companion.USER_ID
import com.zerosdev.mercato.bg.viewmodel.ItemDetailUserViewModel

class ItemDetailUserFragment : Fragment() {

    private var idUser: Int = 0

    companion object {
        fun newInstance(idUser: Int): ItemDetailUserFragment {
            val args = Bundle()
            args.putInt(USER_ID, idUser)
            val fragment = ItemDetailUserFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var viewModel: ItemDetailUserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        idUser = arguments?.getInt(USER_ID) ?: 0
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.item_detail_user_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ItemDetailUserViewModelFactory(idUser)
        )[ItemDetailUserViewModel::class.java]
        viewModel.getUserLiveData().observe(this, Observer {
            applyData(user = it)
        })
    }

    private fun applyData(user: User) {
        // todo ubicar los datos del usuario
    }

}
