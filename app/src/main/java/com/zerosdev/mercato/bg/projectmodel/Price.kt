package com.zerosdev.mercato.bg.projectmodel

data class Price (val min: Int?, val max: Int?)