package com.zerosdev.mercato.bg.adapters

import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.datamodel.ChatListItem
import com.zerosdev.mercato.bg.util.inflate
import kotlinx.android.synthetic.main.chat_list_design.view.*

class ChatListAdapter : PagedListAdapter<ChatListItem, ChatListAdapter.ChatListVH>(ItemComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatListVH =
        ChatListVH(parent.inflate(R.layout.chat_list_design, false))

    override fun onBindViewHolder(holder: ChatListVH, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class ChatListVH(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(chatListItem: ChatListItem) {
            chatListItem.apply {
                itemView.chatListImageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        itemView.context,
                        this.image
                    )
                )
                itemView.nameUserTextView.text = this.name
                itemView.previewChatTextView.text = this.preview
                itemView.itemTitleTextView.text = this.itemTitle
            }
        }

        override fun onClick(view: View?) {

        }
    }

    companion object {
        private val ItemComparator = object : DiffUtil.ItemCallback<ChatListItem>() {
            override fun areItemsTheSame(oldItem: ChatListItem, newItem: ChatListItem): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: ChatListItem, newItem: ChatListItem): Boolean =
                newItem == oldItem
        }
    }

}