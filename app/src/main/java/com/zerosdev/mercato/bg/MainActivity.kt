package com.zerosdev.mercato.bg

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zerosdev.mercato.bg.adapters.PagingItemsAdapter
import com.zerosdev.mercato.bg.base.BaseActivity
import com.zerosdev.mercato.bg.ui.activity.SearchItemActivity
import com.zerosdev.mercato.bg.viewmodel.MainItemViewModel

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : BaseActivity(), View.OnClickListener {

    private val mainItemsAdapter = PagingItemsAdapter()
    private lateinit var mainItemViewModel: MainItemViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        setupRecyclerView()
        setupViewModel()

    }

    private fun setupViewModel() {
        mainItemViewModel = ViewModelProviders.of(this)[MainItemViewModel::class.java]
        mainItemViewModel.getMainItems().observe(this, Observer {
            mainItemsAdapter.submitList(it)
        })
    }

    fun setupRecyclerView() {
        mainRecyclerView.apply {
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            adapter = mainItemsAdapter
            isNestedScrollingEnabled = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(this, SearchItemActivity::class.java)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onClick(view: View?) {

    }
}
