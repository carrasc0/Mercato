package com.zerosdev.mercato.bg.util

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop

fun ImageView.load(url: String) {
    Glide.with(context)
        .load(url)
        .into(this)
}

fun ImageView.load(url: String, placeholder: Int) {
    Glide.with(context)
        .load(url)
        .placeholder(placeholder)
        .into(this)
}

fun ImageView.load(url: Int) {
    Glide.with(context)
        .load(url)
        .into(this)
}

fun ImageView.loadCircular(url: String) {
    Glide.with(context)
        .load(url)
        .transform(CircleCrop())
        .into(this)
}

fun ImageView.loadCircular(url: Int) {
    Glide.with(context)
        .load(url)
        .transform(CircleCrop())
        .into(this)
}