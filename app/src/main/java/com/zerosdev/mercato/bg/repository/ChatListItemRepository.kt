package com.zerosdev.mercato.bg.repository

import com.zerosdev.mercato.bg.api.ApiInterface
import com.zerosdev.mercato.bg.base.BaseRepository
import com.zerosdev.mercato.bg.datamodel.ChatListItem

class ChatListItemRepository(private val apiInterface: ApiInterface) : BaseRepository() {

    suspend fun getChatListItems(page: Int): MutableList<ChatListItem>? {
        return safeApiCall(
            call = {
                apiInterface.getChatListItems(page = page, idUser = 1 /*todo*/).await()
            },
            error = "Error fetching chat list items"
        )?.items?.toMutableList()
    }
}