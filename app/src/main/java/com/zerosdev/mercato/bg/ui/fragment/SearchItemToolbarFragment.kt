package com.zerosdev.mercato.bg.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.base.BaseFragment
import com.zerosdev.mercato.bg.bottomsheet.DepartmentBottomSheetFragment
import com.zerosdev.mercato.bg.bottomsheet.OrderBottomSheetFragment
import com.zerosdev.mercato.bg.util.Constant.Companion.DEP_ID
import com.zerosdev.mercato.bg.util.TAG
import com.zerosdev.mercato.bg.viewmodel.SearchItemViewModel
import kotlinx.android.synthetic.main.fragment_search_item_toolbar.*


class SearchItemToolbarFragment : BaseFragment(), View.OnClickListener {

    private var depId: Int? = null
    private lateinit var searchItemViewModel: SearchItemViewModel

    companion object {
        fun newInstance(department: Int): SearchItemToolbarFragment {
            val fragment = SearchItemToolbarFragment()
            val args = Bundle()
            args.putInt(DEP_ID, department)
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            depId = it.getInt(DEP_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_item_toolbar, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        searchTagDepartment.setOnClickListener(this)
        searchTagOrder.setOnClickListener(this)
        searchTagPrice.setOnClickListener(this)
    }

    private fun setupViewModel() {
        searchItemViewModel = ViewModelProviders.of(this).get(SearchItemViewModel::class.java)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.searchTagDepartment -> DepartmentBottomSheetFragment.newInstance().show(
                fragmentManager!!,
                TAG()
            )
            R.id.searchTagPrice -> TODO()
            R.id.searchTagOrder -> OrderBottomSheetFragment.newInstance().show(
                fragmentManager!!,
                TAG()
            )
        }
    }

}
