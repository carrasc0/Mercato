package com.zerosdev.mercato.bg.bottomsheet

import android.content.Context
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.zerosdev.mercato.bg.R
import com.zerosdev.mercato.bg.projectmodel.Department
import kotlinx.android.synthetic.main.fragment_department_list_dialog.*
import kotlinx.android.synthetic.main.fragment_department_list_dialog_item.view.*

/**
 *
 * A fragment that shows a list of items as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 *    DepartmentBottomSheetFragment.newInstance(30).show(supportFragmentManager, "dialog")
 * </pre>
 *
 * You activity (or fragment) needs to implement [DepartmentBottomSheetFragment.Listener].
 */
class DepartmentBottomSheetFragment : BottomSheetDialogFragment() {
    private var mListener: Listener? = null

    companion object {
        fun newInstance(): DepartmentBottomSheetFragment =
            DepartmentBottomSheetFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_department_list_dialog, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        list.layoutManager = GridLayoutManager(context, 3)
        list.adapter = DepartmentAdapter()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        mListener = if (parent != null) {
            parent as Listener
        } else {
            context as Listener
        }
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
    }

    interface Listener {
        fun onDepartmentClicked(department: Department)
    }

    private inner class DepartmentAdapter internal constructor
        (val departments: ArrayList<Department> = Department.getAllDepartments()) :
        RecyclerView.Adapter<DepartmentAdapter.DepartmentVH>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DepartmentVH {
            return DepartmentVH(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: DepartmentVH, position: Int) {
            holder.bind(departments[position])
        }

        override fun getItemCount() = departments.size


        private inner class DepartmentVH internal constructor(
            inflater: LayoutInflater,
            parent: ViewGroup
        ) : RecyclerView.ViewHolder(
            inflater.inflate(
                R.layout.fragment_department_list_dialog_item,
                parent,
                false
            )
        ) {

            internal val text: TextView = itemView.text

            init {
                text.setOnClickListener {
                    mListener?.let {
                        it.onDepartmentClicked(departments[adapterPosition])
                        dismiss()
                    }
                }
            }

            fun bind(department: Department) {
                text.text = itemView.context.getString(department.name)
                //todo ubicar el icono cuando este disponible
            }
        }

    }


}
